/*
 * Copyright 2019 Simón Oroño
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mcbodev.bitc.test;

import com.mcbodev.bitc.BitArray;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class BitArrayTest {
    @Test
    void basic() {
        final int MAX = 200;
        BitArray ba = new BitArray(MAX);

        assertThat(ba.getCapacity()).isEqualTo(MAX);

        for (int i = 0; i < MAX; i++) {
            ba.set(i, i % 2 == 0);
        }

        int ts = 0;
        int fs = 0;
        for (int i = 0; i < MAX; i++) {
            if (ba.get(i))
                ts++;
            else
                fs++;
        }

        assertThat(ts).isEqualTo(MAX / 2);
        assertThat(fs).isEqualTo(MAX / 2);

        ba.reset();

        for (int i = 0; i < MAX; i++) {
            assertThat(ba.get(i)).isEqualTo(false);
        }
    }

    @Test
    void equality() {
        final int MAX = 5;

        BitArray ba1 = new BitArray(MAX);
        BitArray ba2 = ba1.copy();
        BitArray ba3 = new BitArray(MAX * 2);

        assertThat(ba1).isNotEqualTo(2);
        assertThat(ba1).isNotSameAs(ba2);

        assertThat(ba1).isEqualTo(ba2);
        assertThat(ba1.hashCode()).isEqualTo(ba2.hashCode());

        assertThat(ba1).isNotEqualTo(ba3);
        assertThat(ba1.hashCode()).isNotEqualTo(ba3.hashCode());

        ba2.set(2, true);
        assertThat(ba1).isNotEqualTo(ba2);
        assertThat(ba1.hashCode()).isNotEqualTo(ba2.hashCode());
    }
}
