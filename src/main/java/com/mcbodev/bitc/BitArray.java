/*
 * Copyright 2019 Simón Oroño
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mcbodev.bitc;

import java.util.Arrays;
import java.util.Objects;

/**
 * This class implements an static array of bits. Elements are stored contiguous
 * in memory. Every element has a boolean value.
 *
 * <p>By default every element is set to value {@code false}.
 */
public class BitArray {
    private static final int INT_BITS = 32;

    private int capacity;
    private int[] arr;

    /**
     * Initializes a new BitArray big enough to store {@code capacity} elements.
     * All elements are set to {@code false}.
     *
     * @param capacity the initial size of the BitArray
     */
    public BitArray(int capacity) {
        this.capacity = capacity;
        int size = (int) Math.ceil((double) capacity / (double) INT_BITS);
        arr = new int[size];
    }

    /**
     * @return The capacity of the BitArray.
     */
    public int getCapacity() {
        return capacity;
    }

    /**
     * Gets the value of the element at position {@code index}.
     *
     * @param index position of the element
     * @return The value of the boolean at {@code index}.
     */
    public boolean get(int index) {
        return (arr[index / INT_BITS] & (1 << (index % INT_BITS))) != 0;
    }

    /**
     * Set the element's value at position {@code index} to {@code value}
     *
     * @param index position of the element
     * @param value new value
     */
    public void set(int index, boolean value) {
        if (value) {
            arr[index / INT_BITS] |= (1 << (index % INT_BITS));
        } else {
            arr[index / INT_BITS] &= ~(1 << (index % INT_BITS));
        }
    }

    /**
     * Copies the BitArray.
     *
     * @return a new BitArray instance equal as te caller (with different
     * reference).
     */
    public BitArray copy() {
        BitArray n = new BitArray(0);
        n.capacity = capacity;
        n.arr = Arrays.copyOf(arr, arr.length);
        return n;
    }

    /**
     * Sets all the values of the BitArray to false.
     */
    public void reset() {
        Arrays.fill(arr, 0);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof BitArray) {
            BitArray other = (BitArray) obj;
            return capacity == other.capacity && Arrays.equals(arr, other.arr);
        }
        return false;
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(capacity);
        result = 31 * result + Arrays.hashCode(arr);
        return result;
    }
}
